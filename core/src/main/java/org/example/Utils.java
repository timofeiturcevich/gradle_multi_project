package org.example;

import utils.Library;

public class Utils {
    public static boolean isAllPositiveNumbers(String... str){
        for(String tempt : str){
            if(!Library.isPositiveNumber(tempt)){
                return false;
            }
        }
        return true;
    }
}
